﻿using System;
using System.Collections.Generic;

// Justin Gewecke

namespace Milestone2_InventoryItem
{
    enum State : int
    {
        openingStatement,
        defaultItemScreen,
        customItemScreen,
        viewItemsScreen,
        exitProgram,
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<InventoryItem> itemList = new List<InventoryItem>();
            Console.WriteLine("----Welcome to the inventory management system----");
            Console.WriteLine("----WARNING: This program has no checks for invalid input----");


            bool exitProgram = false;
            State screenState = State.openingStatement;
            while (!exitProgram)
            {
                switch (screenState)
                {
                    // Opening Navigation Screen
                    case State.openingStatement:
                        Console.WriteLine("You currently have " + itemList.Count + " items in the list");
                        Console.WriteLine("1. Create an item with default attributes");
                        Console.WriteLine("2. Create an item with custom attributes");
                        Console.WriteLine("3. View/Modify currently created items");
                        Console.WriteLine("4. Exit the program");

                        Console.Write("Enter a number for input: ");
                        string input = Console.ReadLine();
                        screenState = (State)Int32.Parse(input);
                        break;

                    // Screen where we create a default dummy item
                    case State.defaultItemScreen:
                        itemList.Add(new InventoryItem());
                        Console.Write("A placeholder item was added to the system. Press enter to continue.");
                        Console.ReadLine();
                        screenState = State.openingStatement;
                        break;

                    // Screen where we can create a custom item
                    case State.customItemScreen:
                        InventoryItem item = new InventoryItem();
                        
                        Console.Write("Enter item's name: ");
                        item.Name = Console.ReadLine();

                        Console.Write("Enter item's description: ");
                        item.Description = Console.ReadLine();

                        Console.Write("Enter item's selling price: ");
                        item.Cost = float.Parse(Console.ReadLine());

                        Console.Write("Enter item's id: ");
                        item.ID = Int32.Parse(Console.ReadLine());

                        itemList.Add(item);
                        Console.Write(item.Name + " was added to the system. Press enter to continue.");
                        Console.ReadLine();
                        screenState = State.openingStatement;
                        break;

                    // Screen where we can view and modify objects
                    case State.viewItemsScreen:
                        int number = 1;
                        Console.WriteLine("--------Items--------");
                        foreach(InventoryItem i in itemList)
                        {
                            Console.WriteLine(number + ". " + i.Name + " - " + i.Description);
                            number++;
                        }
                        Console.WriteLine("---------------------");

                        Console.WriteLine("1. Remove item");
                        Console.WriteLine("2. Edit item");
                        Console.WriteLine("3. View more details of item");
                        Console.WriteLine("4. Go back");
                        Console.WriteLine("To modify an item in the list, type the command # followed by the item # or enter 3 to go back to the previous screen");
                        Console.WriteLine("(ex: 2 1 to edit the first item in the list)");

                        string line = Console.ReadLine();
                        string[] numbers = line.Split(' ');
                        int command = Int32.Parse(numbers[0]);
                        int selectedItem = 0;
                        if (numbers.Length > 1)
                            selectedItem = Int32.Parse(numbers[1]);

                        // Do different commands depending on input
                        switch(command)
                        {
                            // Remove item from list
                            case 1:
                                Console.WriteLine("Removed " + itemList[selectedItem - 1].Name + " from the system. Press enter to continue.");
                                itemList.RemoveAt(selectedItem - 1);
                                break;

                            // Modify item in list
                            case 2:
                                Console.Clear();
                                InventoryItem tempItem = itemList[selectedItem - 1];
                                Console.WriteLine("---Item you are modifying---");
                                Console.WriteLine("Name: " + tempItem.Name);
                                Console.WriteLine("Description: " + tempItem.Description);
                                Console.WriteLine("Cost: " + tempItem.Cost);
                                Console.WriteLine("Item ID: " + tempItem.ID);
                                Console.WriteLine("OnFloor: " + tempItem.OnFloor);
                                Console.WriteLine("InBackroom: " + tempItem.InBackroom);
                                Console.WriteLine("Discontinued: " + tempItem.IsDiscontinued);
                                Console.WriteLine("----------------------------");
                                Console.WriteLine("Edit an item's attributes. Leave input empty to keep current attribute unchanged");
                                Console.Write("Enter item's name: ");
                                tempItem.Name = Console.ReadLine();

                                Console.Write("Enter item's description: ");
                                tempItem.Description = Console.ReadLine();

                                Console.Write("Enter item's selling price: ");
                                tempItem.Cost = float.Parse(Console.ReadLine());

                                Console.Write("Enter item's id: ");
                                tempItem.ID = Int32.Parse(Console.ReadLine());

                                Console.Write("Enter onFloor count: ");
                                tempItem.OnFloor = Int32.Parse(Console.ReadLine());

                                Console.Write("Enter inBackroom count: ");
                                tempItem.InBackroom = Int32.Parse(Console.ReadLine());

                                Console.Write("Enter is it discontinued? (true/false): ");
                                tempItem.IsDiscontinued = Boolean.Parse(Console.ReadLine());

                                itemList[selectedItem - 1] = tempItem;
                                Console.WriteLine("The item was modified. Press enter to continue.");
                                break;

                            // View details of item in list
                            case 3:
                                Console.Clear();
                                InventoryItem _tempItem = itemList[selectedItem - 1];
                                Console.WriteLine("---Item you are modifying---");
                                Console.WriteLine("Name: " + _tempItem.Name);
                                Console.WriteLine("Description: " + _tempItem.Description);
                                Console.WriteLine("Cost: " + _tempItem.Cost);
                                Console.WriteLine("Item ID: " + _tempItem.ID);
                                Console.WriteLine("OnFloor: " + _tempItem.OnFloor);
                                Console.WriteLine("InBackroom: " + _tempItem.InBackroom);
                                Console.WriteLine("Discontinued: " + _tempItem.IsDiscontinued);
                                Console.WriteLine("----------------------------");
                                Console.WriteLine("Press enter to return to previous screen.");
                                break;

                            // Returns us to opening screen
                            case 4:
                                screenState = State.openingStatement;
                                Console.Clear();
                                continue;
                        }
                        Console.ReadLine();
                        break;

                    // Screen that exits the program for us
                    case State.exitProgram:
                        exitProgram = true;
                        break;

                }
                Console.Clear();
            }
            Console.WriteLine("Thank you for using my program!");
        }
    }
}
